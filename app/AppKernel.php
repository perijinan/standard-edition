<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),


            new \FOS\UserBundle\FOSUserBundle(),
            new \Doyo\UserBundle\DoyoUserBundle(),
            new \Genemu\Bundle\FormBundle\GenemuFormBundle(),
            new SimpleThings\FormExtraBundle\SimpleThingsFormExtraBundle(),
            new \Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new \Knp\Bundle\MenuBundle\KnpMenuBundle(),

            new \WhiteOctober\PagerfantaBundle\WhiteOctoberPagerfantaBundle(),
            new \Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),


            new \SIP\CoreBundle\SIPCoreBundle(),
            new \SIP\UserBundle\SIPUserBundle(),
            new \SIP\IzinBundle\SIPIzinBundle(),
            new \SIP\PemohonBundle\SIPPemohonBundle(),
            new \SIP\ProgresBundle\SIPProgresBundle(),
            new \SIP\RegisterBundle\SIPRegisterBundle(),
            new \SIP\GammuBundle\SIPGammuBundle(),

            new \SIP\WebBundle\SIPWebBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {

            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
