<?php
$finder = Symfony\CS\Finder\DefaultFinder::create()
    ->notPath('app')
    ->notPath('features')
    ->notPath('build')
    ->notPath('config')
    ->notName("*.xml")
    ->notName('README.md')
    ->notName('.php_cs')
    ->notName('*.yml')
    ->notName('*.twig')
    ->notPath('Spec')
    ->notPath('Resources')
    ->notPath('vendor')
    ->notPath('cache')
    ->in(__DIR__.'/src/SIP');

return Symfony\CS\Config\Config::create()
    ->finder($finder);
