# language: id
@DataPemohon @current
Fitur: Pengolahan Data Pemohon
    Agar dapat memproses pengurusan izin
    Sebagai Front Office
    Saya harus dapat mendaftarkan pemohon

    Dasar:
        Dengan saya tidak memiliki pemohon berikut:
                | nama | Ignatius Kilip |
           Dan saya tidak memiliki pemohon berikut:
                | nama | Fransiskus Mering |
           Dan saya menggunakan aplikasi sebagai administrator
           Dan saya berada di halaman permohonan
           Dan saya mengikuti tautan "Tambah"
           Dan saya mengisi:
                | NIK          | 64.0707.210790.0001 |
                | Nama Badan Usaha | CV. Sendawar jaya Sakti |
                | Nama | Fransiskus Mering   |
           Dan saya menandai pilihan "Laki-Laki"
           Dan saya menandai pilihan "CV"
           Dan saya menekan tombol "Simpan"
           Dan saya berada di halaman permohonan

    Skenario: Mendaftarkan pemohon dengan sukses
       Dengan saya tidak memiliki pemohon berikut:
                | nama | Ignatius Kilip |
          Dan saya mengikuti tautan "Tambah"
          Dan saya mengisi:
                | NIK  | 64.0707.010170.0001 |
                | Nama | Ignatius Kilip      |
                | Nama Badan Usaha | CV. Riam Putratama |
                | Tempat Lahir | Barong Tongkok |
                | NPWP | 01.968.494.3-722.000 |
                | Alamat | Jl. Cut Nyak Dien Gang Gereja No. 1 |
                | Telepon | 0542-5553-5554 |
                | Fax | 0545-732-421 |
                | Email | me@putratama.com |
                | Latitude | 1,2131324123 |
                | Longitude | 1,234123421 |
          Dan saya menandai pilihan "Laki-Laki"
          Dan saya menandai pilihan "CV"
       Ketika saya menekan tombol "Simpan"
         Maka saya harus melihat "Pemohon berhasil ditambahkan"
          Dan saya harus melihat "IGNATIUS KILIP"

    Skenario: Merubah data pemohon dengan sukses
       Dengan saya melihat detail untuk pemohon "FRANSISKUS MERING"
          Dan saya mengikuti tautan "Edit"
          Dan saya mengisi:
                | Tempat Lahir | Tempat lahir edited |
       Ketika saya menekan tombol "Simpan"
         Maka saya harus melihat "Perubahan data Pemohon berhasil disimpan"
          Dan saya harus melihat "TEMPAT LAHIR EDITED"

    Skenario: Menambahkan registrasi izin
       Dengan saya melihat detail untuk pemohon "FRANSISKUS MERING"
          Dan saya mengikuti tautan "Tambahkan Izin"
          Dan saya memilih opsi "Izin Gangguan" pada "sip_registrasi_izin"
          Dan saya menandai pilihan "Baru"
          Dan saya menekan tombol "Simpan"

          Dan saya mengikuti tautan "Tambahkan Izin"
          Dan saya memilih opsi "SIUP" pada "sip_registrasi_izin"
          Dan saya menandai pilihan "Baru"
          Dan saya menekan tombol "Simpan"

          Dan saya mengikuti tautan "Tambahkan Izin"
          Dan saya memilih opsi "SITU" pada "sip_registrasi_izin"
          Dan saya menandai pilihan "Baru"
          Dan saya menekan tombol "Simpan"

          Dan saya mengikuti tautan "Selesaikan Pendaftaran"
          Dan saya menekan tombol "Selesaikan Pendaftaran"

    #Skenario: Menghapus data pemohon dengan sukses
    #   Dengan saya melihat detail untuk pemohon "FRANSISKUS MERING"
    #      Dan saya mengikuti tautan "Hapus"
    #     Maka saya harus melihat "Apakah anda yakin ingin menghapus data Pemohon Izin Berikut?"
    #   Ketika saya menekan tombol "Ya"
    #     Maka saya harus melihat pesan "Pemohon berhasil dihapus"
    #     Tapi saya tidak melihat "FRANSISKUS MERING"