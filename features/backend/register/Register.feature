# language: id
@Register
Fitur: Penentuan Verifikator Izin
    Agar dapat membuat laporan
    Sebagai User
    Saya harus dapat menambahkan register izin

    Dasar:
        Dengan saya menggunakan aplikasi sebagai administrator
           Dan saya berada di halaman register

    Skenario: Menambah Register Izin dengan sukses
        Dengan saya mengikuti tautan "Tambah"
           Dan saya mengisi:
                | Nomor Register | SK/123456/2013 |
           Dan saya menekan tombol "Tambahkan"