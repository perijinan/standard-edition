# language: id
@DaftarIzin
Fitur: Mengelola Daftar Izin
    Agar dapat mengolah data izin dengan baik
    Sebagai administrator
    Saya harus dapat mengelola daftar izin

    Dasar:
        Dengan saya memiliki data izin berikut:
                | nama      | Tes Operasi Izin |
                | singkatan | TOI              |
           Dan saya menggunakan aplikasi sebagai administrator
           Dan saya berada di halaman daftar izin

    Skenario: Menambah izin baru
       Dengan saya tidak memiliki data izin "Izin Mendirikan Tiang"
          Dan saya mengikuti tautan "Tambah"
          Dan saya menandai pilihan "Bidang Perijinan Usaha"
       Ketika saya mengisi:
                | Nama      | Izin Mendirikan Tiang |
                | Singkatan | IMT                   |
          Dan saya menekan tombol "Tambahkan"
         Maka saya harus melihat teks "Izin Mendirikan Tiang"
          Dan saya harus melihat pesan "Izin berhasil ditambahkan"
          Dan saya harus melihat "Pendaftaran Pemohon di loket FO"
          Dan saya harus melihat "Penunjukkan Verifikator Izin"

    Skenario: Mengubah data izin
       Dengan saya mengikuti tautan "Tes Operasi Izin"
          Dan saya mengikuti tautan "Edit"
          Dan saya mengisi:
                | Nama | Tes Operasi Izin Edited |
          Dan saya menandai pilihan "Bidang Perijinan Tertentu"
          Dan saya menekan tombol "Simpan"
         Maka saya harus melihat "Perubahan Izin berhasil disimpan"
          Dan saya harus melihat "Tes Operasi Izin Edited"
          Dan saya harus melihat "Bidang Perijinan Tertentu"

    Skenario: Menghapus izin
       Dengan saya mengikuti tautan "Tes Operasi Izin"
         Maka saya harus melihat "Hapus"

       Ketika saya mengikuti tautan "Hapus"
         Maka saya harus melihat "Apakah anda yakin ingin menghapus izin berikut?"

       Ketika saya menekan tombol "Ya"
         Maka saya harus berada di halaman daftar izin
          Dan saya harus melihat "Izin berhasil dihapus"
          Dan saya tidak melihat "Tes Operasi Izin"

