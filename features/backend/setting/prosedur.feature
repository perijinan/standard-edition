# language: id
@DaftarIzin
Fitur: Prosedur Izin
    Agar dapat memproses permohonan izin
    Sebagai administrator
    Saya harus dapat mengelola data prosedur izin

    Dasar:
        Dengan saya memiliki data izin berikut:
            | nama      | Tes Operasi Izin |
            | singkatan | TOI              |
        Dan saya memiliki data prosedur "Tes Operasi Izin" berikut:
            | status      | Pendaftaran Permohonan Izin |
            | shortStatus | Pendaftaran Permohonan Izin |
            | lamaProses  | 24                          |
        Dan saya menggunakan aplikasi sebagai administrator
        Dan saya berada di halaman daftar izin
        Dan saya mengikuti tautan "Tes Operasi Izin"
        Dan saya mengikuti tautan "Prosedur"


    Skenario: Menambah Prosedur Izin
        Dengan saya tidak memiliki prosedur "Tes Operasi Izin" berikut:
                | status | Validasi Berkas Permohonan Izin |
           Dan saya mengikuti tautan "Prosedur Baru"
          Maka saya harus melihat "Penambahan Prosedur Baru"
        Ketika saya mengisi:
                | Status              | Validasi Berkas Permohonan Izin |
                | Format SMS Status   | Validasi Berkas Permohonan Izin |
           Dan saya menekan tombol "Tambahkan"
          Maka saya harus melihat "Prosedur Izin berhasil ditambahkan"

        Ketika saya mengikuti tautan "Prosedur"
          Maka saya harus melihat "Validasi Berkas Permohonan Izin"


    Skenario: Mengedit Prosedur Izin
       Dengan saya mengikuti tautan "Pendaftaran Permohonan Izin"
       Ketika saya mengisi:
                | Lama Proses | 14 |
          Dan saya menekan tombol "Simpan"

         Maka saya harus melihat "Perubahan Prosedur Izin berhasil disimpan"
          Dan saya harus melihat "14 hari kerja"

    Skenario: Menghapus Prosedur Izin
       Dengan saya mengikuti tautan "Pendaftaran Permohonan Izin"
          Dan saya mengikuti tautan "Hapus"
       Ketika saya menekan tombol "Ya, Saya Yakin"
         Maka saya harus melihat pesan "Prosedur Izin berhasil dihapus"
         Tapi saya tidak melihat "Pendaftaran Permohonan Izin"