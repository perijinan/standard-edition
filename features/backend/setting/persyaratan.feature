# language: id
@DaftarIzin
Fitur: Persyaratan Izin
    Agar dapat memproses permohonan izin
    Sebagai administrator
    Saya harus dapat mengelola data persyaratan izin

    Dasar:
       Dengan saya memiliki data izin berikut:
                | nama      | Tes Operasi Izin |
                | singkatan | TOI              |
          Dan saya memiliki data persyaratan "Tes Operasi Izin" berikut:
                | deskripsi | Fotokopi Sertifikat Tanah |
                | jumlah    | 1                         |
                | satuan    | lembar                    |
          Dan saya menggunakan aplikasi sebagai administrator
          Dan saya berada di halaman daftar izin
          Dan saya mengikuti tautan "Tes Operasi Izin"
          Dan saya mengikuti tautan "Persyaratan"

    Skenario: Menambahkan persyaratan izin
       Dengan saya tidak memiliki persyaratan "Tes Operasi Izin" berikut:
                | deskripsi     | Fotokopi KTP |
          Dan saya mengikuti tautan "Persyaratan Baru"
         Maka saya harus melihat "Persyaratan Baru"
       Ketika saya mengisi:
                | Deskripsi     | Fotokopi KTP |
          Dan saya menekan tombol "Tambahkan"
         Maka saya harus melihat "Persyaratan Izin berhasil ditambahkan"
       Ketika saya mengikuti tautan "Persyaratan"
         Maka saya harus melihat "Fotokopi KTP"


    Skenario: Mengubah persyaratan izin
       Dengan saya mengikuti tautan "Fotokopi Sertifikat Tanah"

       Ketika saya mengisi:
                | Deskripsi | Fotokopi Sertifikat Tanah |
                | Satuan    | Berkas                    |
          Dan saya menekan tombol "Simpan"

         Maka saya harus melihat "Tes Operasi Izin"
       Ketika saya mengikuti tautan "Persyaratan"
         Maka saya harus melihat "Fotokopi Sertifikat Tanah"
          Dan saya harus melihat "Berkas"

    Skenario: Menghapus Persyaratan Izin
       Dengan saya mengikuti tautan "Fotokopi Sertifikat Tanah"
          Dan saya mengikuti tautan "Hapus"
         Maka saya harus melihat "Apakah anda yakin ingin menghapus Persyaratan Izin berikut?"

       Ketika saya menekan tombol "Ya"
         Maka saya harus melihat pesan "Persyaratan Izin berhasil dihapus"
         Tapi saya tidak melihat "Fotokopi Sertifikat Tanah"

