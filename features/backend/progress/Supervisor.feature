# language: id
@Supervisor
Fitur: Penentuan Verifikator Izin
    Agar dapat memproses permohonan izin
    Sebagai Supervisor
    Saya harus dapat menentukan verifikator izin

    Dasar:
        Dengan saya tidak memiliki pemohon berikut:
                | nama | IGNATIUS KILIP |
           Dan saya memiliki data pemohon berikut:
                | nik          | 64.0707.010145.0001 |
                | nama         | IGNATIUS KILIP |
                | tempatLahir  | Barong Tongkok |
                | tanggalLahir | 21-07-1980     |
                | jenisKelamin | 1              |
           Dan saya menggunakan aplikasi sebagai administrator
           Dan saya berada di halaman permohonan
           Dan saya mengikuti tautan "IGNATIUS KILIP"

    Skenario: Berhasil menentukan Verifikator Izin
       Dengan saya mengikuti tautan "Tambahkan Izin"
          Dan saya menandai pilihan "Permohonan Baru"
          Dan saya menandai pilihan "CV"
          Dan saya mengisi isian "SIPPendaftaran[namaPenerima]" dengan "CV. RIAM SAKTI PUTRATAMA"
          Dan saya mengisi isian "SIPPendaftaran[npwp]" dengan "01.968.494.3-722.000"
          Dan saya mengisi isian "SIPPendaftaranAlamat[jalan]" dengan "Jl. Cut Nyak Dien, Gang Gereja No. 1"
          Dan saya mengisi isian "SIPPendaftaranAlamat[rt]" dengan "3"
          Dan saya mengisi isian "SIPPendaftaranAlamat[rw]" dengan "1"
          Dan saya memilih opsi "BARONG TONGKOK" pada "SIPPendaftaranAlamat_kelurahan"
          Dan saya mengisi isian "SIPPendaftaranAlamat[kodepos]" dengan "75576"
          Dan saya memilih opsi "Izin Gangguan" pada "SIPPendaftaran[izin]"
       Ketika saya menekan tombol "Simpan"
         Maka saya harus melihat "CV. RIAM SAKTI PUTRATAMA"
          Dan saya harus melihat "Tentukan Verifikator"
       Dengan saya mengikuti tautan "Tentukan Verifikator"
          Dan saya memilih opsi "verifikator" pada "form[verifikator]"
          Dan saya menekan tombol "Simpan"
         Maka saya tidak melihat "Penunjukan Verifikator Izin"