#language: id
@Verifikator
Fitur:  Pemrosesan Berkas Permohon Izin
    Agar dapat melaksanakan proses pengurusan izin
    Sebagai Verifikator
    Saya harus dapat mengelola administrasi berkas permohonan Izin

    Dasar:
        Dengan saya tidak memiliki pemohon berikut:
                | nama | Ignatius Kilip |
           Dan saya memiliki data pemohon berikut:
                | nik          | 64.0707.010145.0001 |
                | nama         | Ignatius Kilip |
                | tempatLahir  | Barong Tongkok |
                | tanggalLahir | 21-07-1980     |
                | jenisKelamin | 1              |
           Dan saya menggunakan aplikasi sebagai verifikator
           Dan saya berada di halaman permohonan
           Dan saya mengikuti tautan "ANTHONIUS MUNTHI"